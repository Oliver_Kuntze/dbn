package com.dci.intellij.dbn.connection.ssh;

import com.dci.intellij.dbn.common.util.CommonUtil;
import com.jcraft.jsch.ConfigRepository;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class SshTunnelConnector {
    private String proxyHost;
    private int proxyPort;
    private String proxyUser;
    private String proxyPassword;
    private SshAuthType authType;
    private String keyFile;
    private String keyPassphrase;

    private String localHost = "localhost";
    private int localPort;

    private String remoteHost;
    private int remotePort;

    private Session session;
    private Session jumphostSession;

    public SshTunnelConnector(String proxyHost, int proxyPort, String proxyUser, SshAuthType authType, String keyFile, String keyPassphrase, String proxyPassword, String remoteHost, int remotePort) {
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyUser = proxyUser;
        this.proxyPassword = proxyPassword;

        this.authType = authType;
        this.keyFile = keyFile;
        this.keyPassphrase = keyPassphrase;

        this.remoteHost = remoteHost;
        this.remotePort = remotePort;
    }

    public Session createTunnel() throws Exception {
        try {
            ServerSocket serverSocket = new ServerSocket(0);
            try {
                localPort = serverSocket.getLocalPort();
            } finally {
                serverSocket.close();
            }
        } catch (IOException e) {
            throw new JSchException("Can\'t find a free port", e);
        }

        JSch jsch = new JSch();
//        JSch.setConfig("kex", "diffie-hellman-group1-sha1,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1,diffie-hellman-group-exchange-sha256");

        ConfigRepository configRepository = com.jcraft.jsch.OpenSSHConfig.parseFile("/Users/oliverkuntze/.ssh/config");
        File file = new File("/Users/oliverkuntze/.ssh/config");
        System.out.println(FileUtils.readFileToString(file, StandardCharsets.UTF_8));
        jsch.setConfigRepository(configRepository);

        ///
        jumphostSession = jsch.getSession(proxyUser, "none of your business");
        jumphostSession.setConfig("StrictHostKeyChecking", "no");
        jumphostSession.setServerAliveInterval((int) TimeUnit.MINUTES.toMillis(2L));
        jumphostSession.setServerAliveCountMax(1000);
        jumphostSession.connect();
        int proxyHostSshPort = jumphostSession.setPortForwardingL(0, proxyHost, 22);
        session = jsch.getSession(proxyUser, "127.0.0.1", proxyHostSshPort);
        session.setConfig("StrictHostKeyChecking", "no");
        ///

//        session = jsch.getSession(proxyUser, proxyHost, proxyPort);

//        if(authType == SshAuthType.KEY_PAIR) {
//            jsch.addIdentity(keyFile, CommonUtil.nvl(keyPassphrase, ""));;
//        } else {
//            session.setPassword(proxyPassword);
//        }

//        Properties config = new Properties();
//        config.put("StrictHostKeyChecking", "no");
//        config.put("TCPKeepAlive", "yes");
//        session.setConfig(config);
        session.setServerAliveInterval((int) TimeUnit.MINUTES.toMillis(2L));
        session.setServerAliveCountMax(1000);
        session.connect();

        session.setPortForwardingL(localPort, remoteHost, remotePort);
        return session;
    }

    public boolean isConnected() {
        return session != null && session.isConnected();
    }

    public String getLocalHost() {
        return localHost;
    }

    public int getLocalPort() {
        return localPort;
    }
}
